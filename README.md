# PlantPollinatorsNetwork



## The problem

Communities of interacting species, such as plants and pollinators, are systems characterized by a large diversity of both their components (dozens of species are typically involved) and the topology of their interaction networks . The paper *[S. Billiard, H. Leman, T. Rey, V. C. Tran, Continuous limits of large plant-pollinator random networks and some applications]* presents a derivation of a continuous model for such type of systems, and a numerical methods to simulate it numerically. 

This Jupyter Notebook presents an efficient, vectorized Python implementation of such method.



